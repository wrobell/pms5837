#
# pms5837 - MS5837 pressure sensor library
#
# Copyright (C) 2016-2017 by Artur Wroblewski <wrobell@riseup.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import argparse
import asyncio
import pms5837
import time
from datetime import datetime
from functools import partial

parser = argparse.ArgumentParser(description='MS5837 pressure sensor example')
parser.add_argument('device', help='I2C device filename, i.e. /dev/i2c-0')
parser.add_argument(
    'address', type=partial(int, base=16),
    help='I2C device address, i.e. 0x77 (hex value)'
)
args = parser.parse_args()

async def run(sensor):
    while True:
        t1 = time.monotonic()
        pressure = await sensor.read_async()
        t2 = time.monotonic()
        temp = sensor.temperature

        print('{} ({:.4f}): {}bar {}C'.format(
            datetime.now(), t2 - t1, pressure / 10000, temp / 100)
        )

        await asyncio.sleep(1)

loop = asyncio.get_event_loop()
sensor = pms5837.Sensor(args.device, args.address, loop=loop)
loop.run_until_complete(run(sensor))

# vim: sw=4:et:ai
