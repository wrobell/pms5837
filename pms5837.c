/*
 * pms5837 - MS5837 pressure sensor library
 * 
 * Copyright (C) 2016-2017 by Artur Wroblewski <wrobell@riseup.net>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <fcntl.h>
#include <unistd.h>
#include <linux/i2c-dev.h>
#include <errno.h>
#include <stdint.h>
#include <sys/ioctl.h>
#include <sys/timerfd.h>

#ifdef PMS5837_DEBUG
#include <stdio.h>
#define DEBUG_LOG(fmt, ...) fprintf(stderr, fmt, ##__VA_ARGS__)
#else
#define DEBUG_LOG(fmt, ...)
#endif

#define BSWAP16(v) ((v << 8) & 0xFF00) | ((v >> 8) & 0xFF)
#define NUM_COEFF 7

typedef struct {
    int32_t pressure;
    int32_t temperature;

    int fd; /* i2c file descriptor */
    int timer_fd; /* timer file descriptor */
    uint32_t d1;
    uint32_t d2;

    /* PMS5837 calibration data */
    uint16_t coeff[NUM_COEFF];
} pms5837_t;

/*!
 * Calculate pressure and temperature using D1, D2 values read from sensor
 * and the calibration coefficients.
 */
static void pms5837_calculate(pms5837_t *sensor) {
    int32_t d_t, temp, p;
    int64_t off, sens, off_2, sens_2, t_2;
    int64_t c1, c2, c3, c4, c5, c6;

    c1 = sensor->coeff[1];
    c2 = sensor->coeff[2];
    c3 = sensor->coeff[3];
    c4 = sensor->coeff[4];
    c5 = sensor->coeff[5];
    c6 = sensor->coeff[6];

    d_t = sensor->d2 - (c5 << 8);
    temp = 2000 + ((d_t * c6) >> 23);
    off = (c2 << 16) + ((c4 * d_t) >> 7);
    sens = (c1 << 15) + ((c3 * d_t) >> 8);
    p = (((sensor->d1 * sens) >> 21) - off) >> 13;

    DEBUG_LOG("PMS5837: d_t: %d\n", d_t);
    DEBUG_LOG("PMS5837: temp: %d\n", temp);
    DEBUG_LOG("PMS5837: off: %ld\n", off);
    DEBUG_LOG("PMS5837: sens: %ld\n", sens);
    DEBUG_LOG("PMS5837: p: %d\n", p);

    if (temp >= 2000) {
        DEBUG_LOG("PMS5837: temp >= 20\n");
        t_2 = (2 * ((int64_t) d_t * d_t)) >> 37;
        off_2 = ((temp - 2000) * (temp - 2000)) >> 4;
        sens_2 = 0;
    } else {
        DEBUG_LOG("PMS5837: temp < 20\n");
        t_2 = (3 * (int64_t) d_t * d_t) >> 33;
        off_2 = (3 * (temp - 2000) * (temp - 2000)) >> 2;
        sens_2 = (5 * (temp - 2000) * (temp - 2000)) >> 3;

        if (temp < 1500) {
            DEBUG_LOG("PMS5837: temp < 15\n");
            off_2 = off_2 + 7 * (temp + 1500) * (temp + 1500);
            sens_2 = sens_2 + 4 * (temp + 1500) * (temp + 1500);
        }
    }

    temp = temp - t_2;
    off = off - off_2;
    sens = sens - sens_2;

    sensor->pressure = p;
    sensor->temperature = temp;
}

/*
 * Initialize sensor command asynchronous operation. The operation will arm
 * timer to be triggered on timer_fd file descriptor. Use command_end function
 * to finish the operation.
 */
static uint32_t read_value_start(pms5837_t *sensor, uint8_t command) {
    int r;
    struct itimerspec ts;

    DEBUG_LOG(
        "PMS5837: asynchronous operation for command: %x\n", command
    );

    r = write(sensor->fd, (unsigned char[]) {command}, 1);
    if (r != 1)
        return -1;

    ts.it_interval.tv_sec = 0;
    ts.it_interval.tv_nsec = 0;
    ts.it_value.tv_sec = 0;
    ts.it_value.tv_nsec = 18080 * 1000; /* usleep(18080) */
    r = timerfd_settime(sensor->timer_fd, 0, &ts, NULL);
    return r;
}

static int read_value_end(int fd, uint32_t *value) {
    int r;
    uint8_t data[3];
    r = write(fd, (unsigned char[]) {0x00}, 1);
    if (r != 1)
        return -1;

    r = read(fd, data, 3);
    if (r != 3)
        return -1;

    DEBUG_LOG(
        "PMS5837: value for last command: %d\n",
        (data[0] << 16) | (data[1] << 8) | data[2]
    );
    *value = (data[0] << 16) | (data[1] << 8) | data[2];
    return 0;
}

static pms5837_t *pms5837_init(const char *f_dev, unsigned char address) {
    int r, i;

    pms5837_t *sensor = malloc(sizeof(pms5837_t));
    memset(sensor, 0, sizeof(pms5837_t));

    if ((sensor->fd = open(f_dev, O_RDWR)) < 0)
        return NULL;

    /* set the port options and set the address of the device */
    if (ioctl(sensor->fd, I2C_SLAVE, address) < 0)
        return NULL;

    /* reset the sensor */
    r = write(sensor->fd, (unsigned char[]) {0x1e}, 1);
    if (r != 1)
        return NULL;
    usleep(3000);

    /* read the calibration coefficients */
    for (i = 0; i < NUM_COEFF; i++) {
        r = write(sensor->fd, (uint8_t[]) {0xa0 + i * 2}, 1);
        if (r != 1)
            return NULL;

        r = read(sensor->fd, &sensor->coeff[i], 2);
        sensor->coeff[i] = BSWAP16(sensor->coeff[i]);
        DEBUG_LOG("PMS5837: calibration coefficient c%d: %d\n", i, sensor->coeff[i]);
        if (r != 2)
            return NULL;
    }
    sensor->timer_fd = timerfd_create(CLOCK_MONOTONIC, 0);
    return sensor;
}

int pms5837_read_async_pressure(pms5837_t *sensor) {
    int r;

    DEBUG_LOG("PMS5837: read async pressure\n");

    r = read_value_start(sensor, 0x4a);
    return r;
}

int pms5837_read_async_temperature(pms5837_t *sensor) {
    int r;

    DEBUG_LOG("PMS5837: read async temperature\n");

    r = read_value_end(sensor->fd, &sensor->d1);
    if (r == -1)
        return -1;

    r = read_value_start(sensor, 0x5a);
    return r;
}

int pms5837_read_async_end(pms5837_t *sensor) {
    struct itimerspec ts;
    int r;

    DEBUG_LOG("PMS5837: read async finished\n");

    ts.it_interval.tv_sec = 0;
    ts.it_interval.tv_nsec = 0;
    ts.it_value.tv_sec = 0;
    ts.it_value.tv_nsec = 0;
    timerfd_settime(sensor->timer_fd, 0, &ts, NULL);

    r = read_value_end(sensor->fd, &sensor->d2);
    if (r == -1)
        return -1;

    pms5837_calculate(sensor);
    return 0;
}

int pms5837_close(pms5837_t *sensor) {
    int r;
    r = close(sensor->fd);
    if (r < 0)
        return -1;

    r = close(sensor->timer_fd);
    if (r < 0)
        return -1;

    free(sensor);
    return 0;
}

/*
 * vim: sw=4:et:ai
 */
