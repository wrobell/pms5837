#
# pms5837 - MS5837 pressure sensor library
#
# Copyright (C) 2016-2017 by Artur Wroblewski <wrobell@riseup.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import cffi

ffi = cffi.FFI()
ffi.cdef("""
typedef struct {
    int32_t pressure;
    int32_t temperature;

    int fd; /* i2c file descriptor */
    int timer_fd; /* timer file descriptor */
    uint32_t d1;
    uint32_t d2;

    /* PMS5837 calibration data */
    uint16_t coeff[7];
} pms5837_t;

pms5837_t *pms5837_init(const char *, unsigned char);
int pms5837_close(pms5837_t *);

int pms5837_read_async_pressure(pms5837_t *);
int pms5837_read_async_temperature(pms5837_t *);
int pms5837_read_async_end(pms5837_t *);
void pms5837_calculate(pms5837_t *sensor);
""")

ffi.set_source('_pms5837', """
#include "pms5837.c"
""", libraries=[], include_dirs=['.'])

# vim: sw=4:et:ai
