#
# pms5837 - MS5837 pressure sensor library
#
# Copyright (C) 2016-2017 by Artur Wroblewski <wrobell@riseup.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import asyncio
import os

from _pms5837 import ffi, lib

class Sensor(object):
    """
    MS5837 sensor communication interface.
    """
    def __init__(self, f_dev, address, loop=None):
        """
        Initialize pressure sensor and read its calibration coefficients.

        :param f_dev: I2C device filename, i.e. /dev/i2c-0.
        :param address: I2C device address, i.e. 0x77.
        """
        self._data = lib.pms5837_init(f_dev.encode(), address)
        if not loop:
            loop = asyncio.get_event_loop()
        self._loop = loop
        self._task = None
        self._start = False
        loop.add_reader(self._data.timer_fd, self._read_async_end)

    @property
    def temperature(self):
        return self._data.temperature

    async def read_async(self):
        """
        Read pressure and temperature from sensor in asynchronous way.
        """
        task = self._task = self._loop.create_future()
        self._start = True
        lib.pms5837_read_async_pressure(self._data)
        await task
        return task.result()

    def close(self):
        """
        Release resources claimed by sensor.
        """
        lib.pms5837_close(self._data)

    def _read_async_end(self):
        data = self._data
        os.read(data.timer_fd, 8)
        if self._start:
            lib.pms5837_read_async_temperature(data)
            self._start = False
        else:
            lib.pms5837_read_async_end(data)
            self._task.set_result(data.pressure)
            self._task = None

# vim: sw=4:et:ai
