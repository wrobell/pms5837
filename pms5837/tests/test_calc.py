#
# pms5837 - MS5837 pressure sensor library
#
# Copyright (C) 2016-2017 by Artur Wroblewski <wrobell@riseup.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

from _pms5837 import ffi, lib

def test_calc():
    sensor = ffi.new('pms5837_t *')

    sensor.coeff[1] = 34982
    sensor.coeff[2] = 36352
    sensor.coeff[3] = 20328
    sensor.coeff[4] = 22354
    sensor.coeff[5] = 26646
    sensor.coeff[6] = 26146
    sensor.d1 = 4958179
    sensor.d2 = 6815414

    lib.pms5837_calculate(sensor)
    assert 1981 == sensor.temperature
    assert 39998 == sensor.pressure

# vim: sw=4:et:ai
